﻿using UnityEngine;
using System.Collections;
using System.Net.Sockets;

public class wheelScript : MonoBehaviour
{

    public GameObject wheelCollider;
    
	void Start ()
	{
        GetComponent<Transform>().transform.eulerAngles = new Vector3(0, 0, 0);
	}
	
	void FixedUpdate ()
	{
	    GetComponent<Transform>().transform.localEulerAngles = new Vector3(wheelCollider.GetComponent<WheelCollider>().rpm / 60*360*40*Time.deltaTime,
	            wheelCollider.GetComponent<WheelCollider>().steerAngle, wheelCollider.transform.rotation.z);
	        //wheelCollider.GetComponent<Transform>().transform.eulerAngles;
	}
}
