﻿using UnityEngine;
using System.Collections;

public class CameraBehaviour : MonoBehaviour {

	public Transform target;
	public Transform player;

	void Update()
	{
		if (target)
		{
			transform.position = Vector3.Lerp(transform.position, target.position, 20f);
			transform.LookAt(player);
		}
	}
}

