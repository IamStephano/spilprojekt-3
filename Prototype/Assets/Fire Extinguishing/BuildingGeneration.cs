﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BuildingGeneration : MonoBehaviour
{
    public GameObject doorPanel;
    public GameObject windowPanel;
    public GameObject cornerPanel;

    public int length;
    public int depth;
    public int height;

    public float panelLength;
    public float cornerLength;
    public float partHeight;

    Transform builderObj;

    void Start ()
    {
        CreateBuilding();
	}
	
	void CreateBuilding ()
    {
        builderObj = new GameObject().transform;
        builderObj.name = "Builder";
        builderObj.SetParent(transform);

        for (int y = 0; y < height; y++)
        {
            for (int i = 0; i < length; i++)
                SpawnPart(270, windowPanel, panelLength, new Vector3(1, 0));

            SpawnPart(270, cornerPanel, cornerLength + panelLength / 2, new Vector3(1, 0));
            builderObj.position += new Vector3(0, 0, -cornerLength + panelLength / 2);
            for (int i = 0; i < depth; i++)
                SpawnPart(0, windowPanel, panelLength, new Vector3(0, 0, -1));

            SpawnPart(0, cornerPanel, cornerLength + panelLength / 2, new Vector3(0, 0, -1));
            builderObj.position += new Vector3(-cornerLength + panelLength / 2, 0);
            for (int i = 0; i < length; i++)
                SpawnPart(90, windowPanel, panelLength, new Vector3(-1, 0));

            SpawnPart(90, cornerPanel, cornerLength + panelLength / 2, new Vector3(-1, 0));
            builderObj.position += new Vector3(0, 0, cornerLength - panelLength / 2);
            for (int i = 0; i < depth; i++)
                SpawnPart(180, windowPanel, panelLength, new Vector3(0, 0, 1));

            SpawnPart(180, cornerPanel, cornerLength + panelLength / 2, new Vector3(0, 0, 1));

            builderObj.position = new Vector3(0, builderObj.position.y, 0);
            builderObj.position += new Vector3(0, partHeight, 0);
        }
    }

    void SpawnPart(float rot, GameObject part, float length, Vector3 dir)
    {
        builderObj.position += dir * length;
        GameObject go = Instantiate(part, builderObj.position, Quaternion.identity) as GameObject;
        go.transform.eulerAngles = new Vector3(0, rot);
        go.transform.SetParent(transform);
    }
}
