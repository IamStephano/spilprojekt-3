﻿using UnityEngine;
using System.Collections;

public class ObjectScript : MonoBehaviour
{
    public GameObject firePrefab;    //The fire prefab
    [HideInInspector]                //I dont wanna see some gay ass empty field all the time
    public GameObject fire;          //This holds the fire prefab

    public bool burnStart;           //Fire should burn on start
    public bool burnable;            //Is it burnable
    public float heat;               //How hot the object is
    public float heatLimit;          //How hot the object has to be before it catches on fire
    public float maxHeat;            //The max amount of heat the object can have. This will limit the singular fire's size so we dont get a flame so huge you cant tell which object it's coming from.
    public float idleFireGrowthRate; //The rate at which the fire grows when its not being burned anything else
    public float burnMultiplier;     //Used to adjust the rate at which the fire will burn

    void Start()
    {
        //Spawn the fire as child and set everything up
        fire = Instantiate(firePrefab, transform.position, Quaternion.identity) as GameObject;
        fire.transform.SetParent(transform);
        fire.GetComponent<FireScript>().litObj = this;
        fire.SetActive(burnStart); //Fire is only active if object is supposed to start lit
    }

    void FixedUpdate ()
    {
        //If the object is burning let the flame slowly grow even if no other flame is fueling it
        if (fire.activeInHierarchy)
            AdjustHeat(idleFireGrowthRate);
    }

    public void NearbyFire(ObjectScript litObj)
    {
        //Make sure the obj doesnt set fire to itself
        if (this != litObj)
        {
            //Find distance from transform to fire
            float distanceToFire = Vector3.Distance(transform.position, litObj.transform.position);
            //Adjust heat by the fire's heat / the distance to the fire
            float heatAddition = (litObj.heat / distanceToFire) * Time.deltaTime * (burnMultiplier/1000); //divided by 1000 so we dont need to type shit like 0.01
            if (heatAddition > 0) //make sure heat addition isn't less than 0 as one fire cooling another just dont make no sense
                AdjustHeat(heatAddition); //Adjust the heat only if heatAddition is above 0
        }
    }

    public void AdjustHeat(float adj)
    {
        //Only do anything if object is actually burnable
        if (burnable)
        {
            heat += adj;

            if (heat < 0)
                heat = 0;
            else if (heat > maxHeat)
                heat = maxHeat;

            //If heat is above the limit then set the fire to true and if its gets below then set it false
            if (!fire.activeInHierarchy)
            {
                if (heat > heatLimit)
                    fire.SetActive(true);
            }
            else if (fire.activeInHierarchy)
            {
                //If the fire is active scale it by the amount it was adjusted
                fire.transform.localScale += new Vector3(adj, adj, adj) * Time.deltaTime;
                if (heat < heatLimit)
                    fire.SetActive(false);
            }
        }
    }
}
