﻿using UnityEngine;
using System.Collections;

public class Tags : MonoBehaviour
{
    public const string mainCamera = "MainCamera";
    public const string obj = "Object";
    public const string clothes = "Clothes";
    public const string tool = "Tool";
}
