﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CapsuleCollider))]
public class FireScript : MonoBehaviour
{
    public ObjectScript litObj; //Object which is this fire is currently burning

    CapsuleCollider trigger;

    void Start ()
    {
        //Get the trigger and set it to actually be a trigger
        trigger = GetComponent<CapsuleCollider>();
        trigger.isTrigger = true;

        //Rotate obj so fire particles go upwards
        transform.eulerAngles = new Vector3(270,0,0);
    }

	void OnTriggerStay (Collider other)
    {
        //If an object with the objScript is in the trigger then heat it up 
        ObjectScript objScript = other.GetComponent<ObjectScript>();
        if (objScript != null)
            objScript.NearbyFire(litObj);
	}
}
