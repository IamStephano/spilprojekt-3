﻿using UnityEngine;
using System.Collections;

public class FirePutout : MonoBehaviour {
    //public GameObject fire;
    //float particleLifetime = 0.1f;
    public ParticleSystem ps;
    // Use this for initialization
	void Start ()
    {
        //fire = GameObject.Find("Particle System");
        ps = GetComponent<ParticleSystem>();
    }
	
	// Update is called once per frame
	void Update ()
    {
	
	}

    public void Extinguish()
    {
        ps.startLifetime -= 0.01f;
    }
}
