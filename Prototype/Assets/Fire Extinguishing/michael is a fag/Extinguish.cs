﻿using UnityEngine;
using System.Collections;

public class Extinguish : MonoBehaviour
{

    void OnParticleCollision(GameObject other)
    {
        //If other is an object then adjust the heat downwards
        ObjectScript objScript = other.GetComponent<ObjectScript>();
        if (objScript != null)
            objScript.AdjustHeat(-0.1f);

        //If object has rigidbody then apply force to it based on direction
        Rigidbody body = other.GetComponent<Rigidbody>();
        if (body)
        {
            Vector3 direction = other.transform.position - transform.position;
            direction = direction.normalized;
            body.AddForce(direction * 10);
        }
    }
}
