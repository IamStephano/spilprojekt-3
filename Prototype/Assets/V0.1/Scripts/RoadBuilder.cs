﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[ExecuteInEditMode]
public class RoadBuilder : MonoBehaviour {

	public GameObject[] roadTypes;
	public GameObject[] attachmentPoints;

	public GameObject tempRoad;

	public int currentPoint = 0;
	public int currentAttachmentPoint = 0;
	public int numberOfAttachmentPoints;
	public int currentRoadType = 0;

	//Index has to be the same as attachmentpoint
	public GameObject[] connectedRoads;

	public void SelectAttachmentPoint() {
		if (attachmentPoints.Length >= currentAttachmentPoint+1)
			currentAttachmentPoint++;
		else
			currentAttachmentPoint = 0;
	}

	public void BuildRoad() {
		connectedRoads[currentAttachmentPoint] = tempRoad;
		tempRoad = null;

	}

	public void ChangeRoadType() {
		if (roadTypes.Length > currentRoadType+1)
			currentRoadType++;
		else
			currentRoadType = 0;

		DestroyImmediate(tempRoad);
		tempRoad = Instantiate(roadTypes[currentRoadType], 
			attachmentPoints[currentAttachmentPoint].transform.position,
			attachmentPoints[currentAttachmentPoint].transform.rotation) as GameObject;

		if (tempRoad == null) return;	
		var tempChild = tempRoad.transform.GetChild(1);
		var tempParent = tempRoad.transform.GetChild(1).parent;
		tempRoad.transform.GetChild(1).parent = null;
		tempParent.parent = tempChild;
		tempChild.position = attachmentPoints[currentAttachmentPoint].transform.position;
		tempParent.parent = null;
		tempChild.parent = tempParent;
	}

	public void DeleteTempRoad() {
		
	}

	public void Update() {
		Debug.Log(Selection.activeGameObject);
		if (Selection.activeGameObject != gameObject)
			if (tempRoad != null) {
				DestroyImmediate(tempRoad);
				tempRoad = null;
			}
	}

}
