﻿using UnityEngine;
using System.Collections;

public class TrafficLightBehaviour : MonoBehaviour {

	public enum State {
		Red, Yellow, Green
	}

	public enum Type {
		Tsection, Xsection
	}

	public Transform[] waypointsDir1;
	public Transform[] waypointsDir2;
	private Type _type;

	public float toggleTime = 20f;
	public float toggleTimer = 0f;

	public TrafficLightBehaviour(Type type) {
		_type = type;
	}

	private void Start()
	{
		SetState(waypointsDir1, State.Red);	
		SetState(waypointsDir2, State.Green);
		toggleTimer = Random.Range(0, 20);
	}

	private State GetState(Transform[] dir) {
		return dir[0].GetComponent<Waypoint>().isRed ? State.Red : State.Green;
	}

	private void SetState(Transform[] dir, State state) {
		for(var i=0; i < dir.Length; i++)
		{
			switch (state) {
				case State.Green:
					dir[i].GetComponent<Waypoint>().isRed = false;
					dir[i].GetComponent<Light>().color = Color.green;
					break;
				case State.Yellow:
					dir[i].GetComponent<Waypoint>().isRed = true;
					dir[i].GetComponent<Light>().color = Color.yellow;
					break;
				case State.Red:
					dir[i].GetComponent<Waypoint>().isRed = true;
					dir[i].GetComponent<Light>().color = Color.red;
					break;
			}
		}
	}

	private void Update() {
		if (toggleTimer >= toggleTime) {
			StartCoroutine("ChangeLights");
			toggleTimer = 0;
		} else {
			toggleTimer += Time.deltaTime;
		}
			
	}

	private IEnumerator ChangeLights() {
		var dir1State = GetState(waypointsDir1);

		SetState(waypointsDir1, State.Yellow);
		SetState(waypointsDir2, State.Yellow);

		yield return new WaitForSeconds(2f);

		if (dir1State == State.Green) {
			SetState(waypointsDir1, State.Red);
			SetState(waypointsDir2, State.Green);
		} else {
			SetState(waypointsDir1, State.Green);
			SetState(waypointsDir2, State.Red);
		}
	}



	
}
