﻿using UnityEngine;
using System.Collections;

public class AIBehaviour : MonoBehaviour {

	public Transform lastWaypoint;
	public Transform currentWaypoint;
	public float actualSpeed;
	public float speed;
	public bool redLight;
	public float distance = 4.0f;
	private Vector3 direction;
	private Quaternion neededRotation;
	private float damping;
	public float reactionTime = 3;
	public double reactionTimer;

	private void FixedUpdate() {

		//get the vector from your position to current waypoint
		direction = currentWaypoint.position - transform.position;

		//check our distance to the current waypoint
		if (direction.magnitude < distance) {
			if (!currentWaypoint.GetComponent<Waypoint>().isRed)
				GetNewWaypoint();
			else
				actualSpeed = 0;
		} else {
			//Set speed
			actualSpeed = !CheckCarAhead() ? speed : 0;
		}

		//Turn Towards Waypoint
		var rotation = Quaternion.LookRotation(currentWaypoint.position - transform.position);
		transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * damping);
		//transform.rotation = new Quaternion(0,transform.rotation.eulerAngles.y,0,0);

		//Move Car
		GetComponent<Rigidbody>().velocity = transform.forward*actualSpeed;


		if (reactionTimer < reactionTime) reactionTimer += Time.deltaTime;

	}

	private bool CheckCarAhead() {
		if (reactionTimer < reactionTime) return true;

		RaycastHit hit;
		Ray rayForward = new Ray(transform.position, transform.forward);
		Ray rayForwardLeft = new Ray(transform.position, transform.forward - transform.right);
		Ray rayForwardRight = new Ray(transform.position, transform.forward + transform.right);

		if (Physics.Raycast(rayForward, out hit) && hit.collider.name.Contains("Car") && hit.distance < 2f) {
			reactionTimer = 0;
			Debug.DrawLine(rayForward.origin, hit.point, Color.red);
			return true;
		}

		if (Physics.Raycast(rayForwardRight, out hit) && hit.collider.name.Contains("Car") && hit.distance < 2f)
		{
			reactionTimer = 0;
			Debug.DrawLine(rayForwardRight.origin, hit.point, Color.red);
			return true;
		}

		if (Physics.Raycast(rayForwardLeft, out hit) && hit.collider.name.Contains("Car") && hit.distance < 2f)
		{
			reactionTimer = 0;
			Debug.DrawLine(rayForwardLeft.origin, hit.point, Color.red);
			return true;
		}

		return false;
	}

	private void GetNewWaypoint() {
		//Turn faster on straight roads
		if (currentWaypoint.GetComponent<Waypoint>().type == Waypoint.Type.End) {
			lastWaypoint = currentWaypoint;
			currentWaypoint = currentWaypoint.GetComponent<Waypoint>().next;
			damping = 4f;
			speed = 5f;
		} else {
			lastWaypoint = currentWaypoint;
			currentWaypoint = currentWaypoint.GetComponent<Waypoint>().directions[
				Random.Range(0, currentWaypoint.GetComponent<Waypoint>().directions.Length)];
			damping = 1.5f;
			speed = 3f;
		}
		neededRotation = Quaternion.LookRotation(currentWaypoint.position - transform.position);
	}

	//Stop on collision with another car
	private void OnCollisionEnter(Collision other) {
		actualSpeed = 0;
		other.collider.GetComponent<AIBehaviour>().actualSpeed = 0;
		Debug.Log("Car Crash");
	}


}
