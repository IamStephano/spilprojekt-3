﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(RoadBuilder))]
public class RoadBuilderEditor : Editor {

	public override void OnInspectorGUI() {
		DrawDefaultInspector();
		EditorUtility.SetDirty(target);

		RoadBuilder roadBuilder = (RoadBuilder) target;
		if (GUILayout.Button("Build Road")) {
			roadBuilder.BuildRoad();
		}
		if (GUILayout.Button("Change Attachmentpoint"))
		{
			roadBuilder.SelectAttachmentPoint();
		}
		if (GUILayout.Button("Change Road Type"))
		{
			roadBuilder.ChangeRoadType();
		}

		
		
	}


}
