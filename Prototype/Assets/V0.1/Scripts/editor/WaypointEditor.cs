﻿//using UnityEditor;
//using UnityEngine;

//[CustomEditor(typeof(Waypoint)), CanEditMultipleObjects]
//public class WaypointEditor : Editor
//{

//	public SerializedProperty
//		type_Prop,
//		rightTurn_Prop,
//		leftTurn_Prop,
//		next_Prop,
//		forward_Prop;


//	void OnEnable()
//	{
//		// Setup the SerializedProperties
//		type_Prop = serializedObject.FindProperty("type");
//		rightTurn_Prop = serializedObject.FindProperty("rightTurn");
//		leftTurn_Prop = serializedObject.FindProperty("leftTurn");
//		forward_Prop = serializedObject.FindProperty("forward");
//		next_Prop = serializedObject.FindProperty("next");
//	}

//	public override void OnInspectorGUI()
//	{
//		serializedObject.Update();

//		EditorGUILayout.PropertyField(type_Prop);

//		Waypoint.Type st = (Waypoint.Type)type_Prop.enumValueIndex;

//		switch (st)
//		{
//			case Waypoint.Type.Road:
//				EditorGUILayout.PropertyField(forward_Prop, new GUIContent("forward"));
//				break;

//			case Waypoint.Type.Corner:
//				EditorGUILayout.PropertyField(rightTurn_Prop, new GUIContent("right turn"));
//				EditorGUILayout.PropertyField(leftTurn_Prop, new GUIContent("left turn"));
//				break;

//			case Waypoint.Type.Tsection:
//				EditorGUILayout.PropertyField(forward_Prop, new GUIContent("forward"));
//				EditorGUILayout.PropertyField(rightTurn_Prop, new GUIContent("right turn"));
//				EditorGUILayout.PropertyField(leftTurn_Prop, new GUIContent("left turn"));
//				break;
//			case Waypoint.Type.Xsection:
//				EditorGUILayout.PropertyField(forward_Prop, new GUIContent("forward"));
//				EditorGUILayout.PropertyField(rightTurn_Prop, new GUIContent("left turn"));
//				EditorGUILayout.PropertyField(leftTurn_Prop, new GUIContent("right turn"));
//				break;
//			case Waypoint.Type.End:
//				EditorGUILayout.PropertyField(forward_Prop, new GUIContent("next"));
//				break;
//		}

//		serializedObject.ApplyModifiedProperties();
//	}
//}