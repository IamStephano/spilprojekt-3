﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(MeshFilter))]
public class MeshFilterEditor : Editor {

	public override void OnInspectorGUI() {
		DrawDefaultInspector();
		EditorUtility.SetDirty(target);
	}
}
