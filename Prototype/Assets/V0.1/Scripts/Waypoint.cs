﻿using UnityEngine;
using System.Collections;


[ExecuteInEditMode]
public class Waypoint : MonoBehaviour
{
	public enum Type
	{
		Road, Tsection, Xsection, Corner, End
	}

	public Type type;
	public Transform[] directions;

	public Transform rightTurn;
	public Transform leftTurn;
	public Transform forward;
	public Transform next;

	public bool isRed;

	private void FixedUpdate() {
		RaycastHit hit;
		Ray ray = new Ray(transform.position, transform.forward);
		//Vector3 fwd = transform.TransformDirection(Vector3.left);
		if (type == Type.End) {
			if (Physics.Raycast(ray, out hit)) {
				if (hit.collider.name.Contains("waypoint")) {
					if (next == null) {
						next = hit.collider.transform;
						Debug.Log(next);
					} else {
						
					}


					Debug.DrawLine(ray.origin, hit.point, Color.green);
				}
			}
		}
	} 


	


}
