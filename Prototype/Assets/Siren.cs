﻿using UnityEngine;
using System.Collections;

public class Siren : MonoBehaviour {

	float toggleTime = .2f;
	float toggleTimer = 0;

	public Light blueLight;
	public Light redLight;

	void Update() {
		if (toggleTimer > toggleTime) {
			ChangeLights();
			toggleTimer = 0;
		}
		else {
			toggleTimer += Time.deltaTime;
		}
	}

	void ChangeLights() {
		if (blueLight.enabled) {
			redLight.enabled = true;
			blueLight.enabled = false;
		}
		else {
			redLight.enabled = false;
			blueLight.enabled = true;
		}
	}
}
