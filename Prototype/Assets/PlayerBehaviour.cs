﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Cameras;

public class PlayerBehaviour : MonoBehaviour {

	public enum Status {
		Driving, Walking
	}

	public Status status = Status.Driving;
	public GameObject playerObj;
	public Transform playerSpawn;
	public carController firetruck;
	private GameObject player;

	void Update () {

		if (Input.GetKeyDown(KeyCode.E)) {
			if (status == Status.Driving) {
				player = Instantiate(playerObj, playerSpawn.position, playerSpawn.rotation) as GameObject;
				GetComponent<AutoCam>().SetTarget(player.transform);
				firetruck.enabled = false;
				transform.GetChild(0).position = new Vector3(transform.GetChild(0).position.x + 2,
					transform.GetChild(0).position.y - 2.5f, transform.GetChild(0).position.z);
				status = Status.Walking;
			} else if (Vector3.Distance(player.transform.position, firetruck.GetComponent<Transform>().position) < 4) {
				Destroy(player);
				GetComponent<AutoCam>().SetTarget(firetruck.transform);
				firetruck.enabled = true;
				transform.GetChild(0).position = new Vector3(transform.GetChild(0).position.x - 2,
					transform.GetChild(0).position.y + 2.5f, transform.GetChild(0).position.z);
				status = Status.Driving;

			}
		}
	}
}
 